package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class BookQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Quoter quoter;
	private Calculator calculator;


	
    public void init() throws ServletException {
    	quoter = new Quoter();
		calculator = new Calculator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

//        response.setContentType("text/html");
//	  PrintWriter out = response.getWriter();
//      String docType =
//       "<!DOCTYPE HTML>\n";
//	  String title = "Temperature";
//	  out.println(docType +
//			  "<HTML> \n"
//	  + "<HEAD><TITLE>" + title + "</TITLE>" +
//			                  "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
//                		"</HEAD>\n" +
//                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
//                "<H1>" + title + "</H1>\n" +
//                "  <P>Temperature in Celsius number: " +
//                   request.getParameter("temperature") + "\n" +
//                "  <P>Temperature in Fahrenheit: " +
//                   Double.toString(calculator.calculateFahrenheit(request.getParameter("temperature"))) +
//                "</BODY></HTML>");

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Book Quote";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>ISBN number: " +
                   request.getParameter("isbn") + "\n" +
                "  <P>Price: " +
                   Double.toString(quoter.getBookPrice(request.getParameter("isbn"))) +
                "</BODY></HTML>");
  }
  

}
