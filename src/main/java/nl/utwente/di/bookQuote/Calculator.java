package nl.utwente.di.bookQuote;

public class Calculator {

    public double calculateFahrenheit(String celsius) {
        return 1.8*Double.parseDouble(celsius) + 32;
    }
}
